import numpy as np
from random import shuffle

def softmax_loss_naive(W, X, y, reg):
  """
  Softmax loss function, naive implementation (with loops)

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using explicit loops.     #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################  

  scores = X.dot(W)
  scores -= np.max(scores)
  input_count = X.shape[0]
  class_count = W.shape[1]

  calcTerm = lambda i, j: np.exp(scores[i][j]) / np.sum(np.exp(scores[i]))

  for i in np.arange(input_count):
  	loss -= np.log(calcTerm(i, y[i]))

  for i in np.arange(input_count):
  	for j in np.arange(class_count):
  		dW[:, j] += calcTerm(i, j) * X[i]
  	dW[:, y[i]] -= X[i]

  loss /= input_count
  dW /= input_count

  loss += 0.5 * reg * np.sum(W * W)
  dW += reg * W
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
  """
  Softmax loss function, vectorized version.

  Inputs and outputs are the same as softmax_loss_naive.
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  
  scores = X.dot(W)
  scores -= np.max(scores)

  input_count = X.shape[0]
  class_count = W.shape[1]
  dim_count = X.shape[1]

  down = np.sum(np.exp(scores), axis=1)
  
  loss += np.sum(np.log(down))
  loss -= np.sum(scores[np.arange(input_count), y])

  exp_arr = np.exp(scores.T)
  exp_arr /= down

  dW += exp_arr.dot(X).T

  # :(

  sumX = np.zeros_like(W.T)
  for i in np.arange(input_count):
  	sumX[y[i]] += X[i]

  dW -= sumX.T

  loss /= input_count
  dW /= input_count

  loss += 0.5 * reg * np.sum(W * W)
  dW += reg * W

  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW

